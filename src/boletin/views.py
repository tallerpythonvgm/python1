from django.shortcuts import render
from .forms import RegForm
from .models import Registrado
# Create your views here.

def inicio(request):
    form = RegForm()
    contexto = {
        "el_formulario": form,
    }
    return render(request, "inicio.html", contexto)

def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        email2 = form_data.get("email")
        objeto = Registrado.objects.create(nombre=nombre2, email=email2)
         # OTRA FORMA DE GUARDAR LOS OBJETOS:
         #objeto = Registrado()
         #objeto.nombre = nombre2
         #objeto.email = email2
         #objeto.save()
    contexto = { "el_formulario": form,
    }
    return render(request, "inicio.html", contexto)