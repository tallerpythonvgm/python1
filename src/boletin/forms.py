from django import forms
from .models import Registrado


class RegModelForm(forms.ModelForm):
 class Meta:
     modelo = Registrado
     campos = ["nombre", "email"]

class RegForm(forms.Form):
 # RegForm es el nombre para el registro del formulario
 nombre = forms.CharField(max_length=100)
 email = forms.EmailField()

class RegModelForm (forms.ModelForm):
    class Meta:
        modelo = Registrado
        campos = ["nombre", "email"]

    def clean_email(self):
        email = self.cleaned_data.get("email")
        email_base, proveedor = email.split("@")
        dominio, extension = proveedor.split(".")

        if not extension == "edu":
            raise forms.ValidationError("Solo se aceptan correos con extensión .EDU")
        return email

    def clean_nombre(self):
        name = self.cleaned_data.get("nombre")
        if name:
            if len(name) = 0:
                raise forms.ValidationError("No se admite nulo")
            return name
        else:
            raise forms.ValidationError("El campo no debe ser vacio")

