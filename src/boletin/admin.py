from django.contrib import admin
from .models import Registrado
from .forms import RegModelForm, RegForm


class AdminRegistrado(admin.ModelAdmin):
    list_display = ["nombre", "email", "fecha"]
    form = RegModelForm
    list_filter = ["fecha"]
    list_editable = ["email"]
    search_fields = ["nombre", "email"]

    #class Meta:
        #model = Registrado


# Register your models here.

admin.site.register(Registrado, AdminRegistrado)

